from django.contrib import admin
from .models import Learner, Tutor

# Register your models here.
admin.site.register(Learner)
admin.site.register(Tutor)