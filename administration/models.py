from django.db import models

class Member(models.Model):
    first_name = models.CharField(max_length = 30)
    last_name = models.CharField(max_length = 30)
    email_address = models.EmailField()
    cell_number = models.CharField(max_length = 20)
    password = models.CharField(max_length = 30)
    profile_picture = models.CharField(max_length = 30)
    date_joined = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.first_name + "," + self.last_name

class Learner(Member):    
    grade = models.CharField(max_length = 10)
    school = models.CharField(max_length = 50)


class Tutor(Member):
    salary = models.DecimalField(decimal_places=2, max_digits=10)
    occupation = models.CharField(max_length = 30)




